---
layout: default
permalink: /media
---

# Join us... for the extraordinaire!

<iframe src="https://archive.org/embed/cise-trailer-1-final" width="640" height="480" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>

[Participate in our May 2020 Extraordinaire!](/event){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 }


