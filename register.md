---
layout: default
##permalink: /register
---

# Register for the Symposium Extraordinaire

Registration is closed!

## Other Ways to Stay in the Loop!

* Join the [CleanInsights Announce Email List](https://lists.mayfirst.org/mailman/listinfo/cleaninsights-announce)
* Follow us on Twitter: [Clean Insights](https://twitter.com/dataistoxic/) and [Guardian Project](https://twitter.com/guardianproject)
* Listen and Subscribe to [En Garde! The Guardian Project Podcast](https://guardianproject.info/podcast/)
