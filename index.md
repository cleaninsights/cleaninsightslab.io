---
layout: default
title: Home
nav_order: 1
description: "Clean Insights gives developers a way to plug into a secure, private measurement platform."
permalink: /
---

# Privacy-Preserving Measurement
{: .fs-9 }

Clean Insights gives developers a way to plug into a secure, private measurement platform. It is focused on assisting in answering key questions about app usage patterns, and not on enabling invasive surveillance of all user habits. Our approach provides programmatic levers to pull to cater to specific use cases and privacy needs. It also provides methods for user interactions that are ultimately empowering instead of alienating. 
{: .fs-6 .fw-300 }

[Learn More](/about){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [Review the May 2020 Symposium](/event){: .btn .fs-5 .mb-4 .mb-md-0 }

---

## What We Are Building

* **Software Development Kits** for Android, iOS, Javascript and Python based on existing open-source libraries; enables measurement features for gathering of usage data to safely, privately store and transmit to the server for analysis

* **Consent User Interface Toolkit**: Apps use Consent UI to inform end-users about why and how usage data is being collected and provide usable interactive elements for users to adjust their privacy settings.

* **Server Plugins**: available for server-side into open-source measurement systems (Matomo): Receives report submissions, ingests and processes the data based on privacy-preserving guidelines and configurations

[Learn more on our developer page](/dev)

---

## About the Team

Clean Insights was initially [conceived of and designed by a team of fellows](https://www.bkmla.org/fellowship-2017projects) at the [Assembly Program](https://www.bkmla.org/), a program of the [Berkman-Klein Center for Internet and Society, at Harvard University](https://cyber.harvard.edu/). Since then, the work has found a home at the [Guardian Project](https://guardianproject.info).

Guardian Project’s work developing and promoting open-source privacy enhancing technologies for mobile devices, such as [SQLCipher Secure Database](https://sqlcipher.net) and [NetCipher Secure Network Toolkit](https://guardianproject.info/code/netcipher), has already been widely adopted in thousand of apps, impacting 100 millions of users (through adoption by groups like Facebook, Tencent, Salesforce & Signal). We have experience and knowledge of how both to advocate for users, engage developers and product teams, and work to maintain and measure our impact in the world.

---

### Code of Conduct

Clean Insights is committed to fostering a welcoming community.

[View our Code of Conduct](https://gitlab.com/cleaninsights/cleaninsights.gitlab.io/-/blob/master/CODE-OF-CONDUCT.md) on our GitHub repository.

