---
layout: default
permalink: /event-activity-dear-data
---

# Make Your Own Dear Data Postcard
## *A Measured Mindset is a Leak-Defying Feat*

*For this activity, you can view it as an image below, or visit the link to Okthanks site, which is hosted on a third-party site and includes third-party trackers*

* [View the Full Activity Page](https://okthanks.com/blog/2020/5/13/a-measured-mindset)
* [Visit the original "Dear Data" project page](http://giorgialupi.com/dear-data)

Sharing is optional, but we want to celebrate our creativity and intentionality behind data collection and discover together what insights we gain by collecting and sharing data sets with others. Please consider sharing a picture of your Dear Data visualization and legend one of the following ways: 

*    Upload your photo in the Matrix discussion channel: [#cleaninsights-discussion:matrix.org](https://riot.im/app/#/room/#cleaninsights-discussion:matrix.org)
*    Share a post on twitter and tag @guardianproject #cleaninsights
*    Send your photo to Okthanks via signal +1 209.396.5087
*    Send an email to [tiffany@okthanks.com](mailto:tiffany@okthanks.com)

[![activity](assets/images//cise-activity-1.jpg)](https://okthanks.com/blog/2020/5/13/a-measured-mindset)


