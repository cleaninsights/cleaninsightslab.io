---
layout: default
title: About the Team
nav_order: 2 
description: "The Clean Insights Team"
permalink: /team
---

*(alphabetical order)*

### Benjamin Erhart, Lead Developer
Benjamin fell in love with computers when he was a teenager and still is today after over 20 years. His passion is to create tangible software for end users, breathing life into designer's visions. He’s happiest when some healthy technical challenges and a higher purpose are stirred into the mix. Currently, he feels most at home developing mobile platforms, but has a long history in web tech, and a knack for security-related topics, which makes him well-suited for this era. He is self-employed and works out of his office in Salzburg, Austria.

### Nathan Freitas, Project Lead
Nathan is the founder and director of Guardian Project, an award-winning, open-source, mobile security collaborative with millions of users and beneficiaries worldwide. Their most well known app is Orbot, which brings the Tor anonymity and circumvention network to Android devices, and has been installed more than 20 million times. In late 2017, he co-designed with Edward Snowden, an app called Haven, which works as a personal security system that puts the power of surveillance back into the hands of the most vulnerable and under threat.
His work on off-grid, decentralized, secure mobile communication networks, dubbed Wind, was originally imagined and workshopped while a fellow at the Berkman-Klein Center in 2015. In 2018, Wind was selected as a finalist in the Mozilla-National Science Foundation "Wireless Innovation for a Networked Society (WINS)" Challenges.

### Gina Helfrich, Outreach and Sustainability
Dr. Gina Helfrich is Program Officer for Global Technology at Internews.
Previously, she served as Director of Communications and Culture at NumFOCUS, a non-profit that supports better science through open code. Gina is an accomplished and visionary leader with a track record of success across a variety of fields, including nonprofits, higher education, and business. She was co-founder of recruitHER, a women-owned recruiting & consulting firm committed to making the tech industry more inclusive and diverse. The brand strategy Gina developed and executed for recruitHER quickly earned national attention and a list of high-profile clients including Pandora, GitHub, Pinterest, and RunKeeper. She earned press features including stories in both Austin Monthly and Austin Woman magazines, a talk at SXSW, and an interview on the Stuff Mom Never Told You podcast.

### Tiffany Robertson, Community Ambassador
Tiffany is committed to justice. Working around the globe, she has accumulated diverse experiences that contribute to her effort to build awareness and support around social justice issues.
In Rwanda, Tiffany worked to empower women to build sustainable businesses with Keza, an ethical fashion venture. In London, she produced marketing profiles, improved member relations and helped coordinate the SOURCE Summit for the Ethical Forum Fashion. In Tennessee, she brightened days as a barista and social media marketing assistant with Sunnyside, a startup drive-thru coffee hut. In Ningbo, China, she taught English as a second language. In Alaska, Tiffany led visitors through the state’s terrain as a driver guide. Through each experience, Tiffany works with a purpose to understand how to develop better ways to communicate unfair issues to build support around social justice by companies and consumers.

### Hans-Christoph Steiner, Privacy Researcher
Hans-Christoph Steiner spends his time making private communications software usable, designing interactive software with a focus on human perceptual capabilities, building networks with free software, and composing music with computers. With an emphasis on collaboration, he has worked in many forms, including free software for mobile and embedded devices, responsive sound environments, free wireless networks that help build community, musical robots that listen, programming environments allow people to play with math, and a jet-powered fish that you can ride.
To further his research, he teaches and works at various media art centers and organizes open, collaborative hacklabs and barcamp conferences. He is currently building encrypted, anonymous communications devices as part of the Guardian Project as well as teaching courses in interaction design and media programming NYU's Interactive Telecommunications Program and workshops around the world.

### Carrie Winfrey, Consent User Experience Lead
Founder of Okthanks, Design lead at Guardian Project - An Interaction Designer by trade, Carrie works hand-in-hand with teams to craft clear, effective user experiences and brand messages. She has over 8 years of experience specializing in User Experience (UX) and User Interface (UI) design for mobile and web-based applications. She has worked with multiple startups to name their products and services, and to produce strong visual brand identities.  


