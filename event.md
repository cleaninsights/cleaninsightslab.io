---
layout: default
title: Symposium Extraordinaire 
nav_order: 5
description: "Symposium Extraordinaire"
permalink: /event
---

# May 2020 Symposium Extraordinaire!

![Event Ticket](assets/images/ciseticket2020.jpg)

*We are living in a time like never before. A time no one has seen, experienced or gone through. There are no experts, no tested measures of success, no right or wrong way to do this. Now is the perfect time for embracing novel ideas!* 

[View our Code of Conduct](https://gitlab.com/cleaninsights/cleaninsights.gitlab.io/-/blob/master/CODE-OF-CONDUCT.md)

### Symposium Extraordinaire: History of Updates!

[View the Full, Extraordinaire Schedule](/event-schedule)

* June 5th ["Learning from Open-Source Tool Teams about Privacy Preserving Measurement”](https://cleaninsights.org/event-schedule#friday-3)
* June 2nd ["Draw Data with Qubes OS"](https://cleaninsights.org/event-schedule#tuesday-3)
* May 29th ["Modeling the Threats of Measurement"](https://cleaninsights.org/event-schedule#friday-2)
* May 27th ["Walking the Consent Tight-Rope!"](https://cleaninsights.org/event-schedule#wednesday-2)
* May 22nd ["Draw Data with Me"](https://cleaninsights.org/event-schedule#friday-1) 
* May 20th ["Ethics in Computer Science" Podcast Panel](https://guardianproject.info/podcast/2020/cleaninsights-ethics-in-compsci.html)

--- 
In May 2020, we embarked on a new collaborative experience  to develop, design and discuss ethical analytics and privacy-preserving measurement! Now is the time to consider new ways to measure your applications and services to gain valuable insights, while not violating the trust and dignity of those you seek to benefit.

This is a chance to collectively enhance the practice of  measurement and analytics, by approaching the process in a new way!

In particular, we are looking for those with the following skills or experience: App and Service Developers, Experience with Analytics, Human Centered UX/UI Design, Data Researchers, Analyst & Scientists, Open source projects, Privacy and Security Experts, Anyone who is passionate about this topic, Anyone who could benefit from ethical analytics and privacy-preserving measurement.



---



