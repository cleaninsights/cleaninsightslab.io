---
layout: default
title: Developer Access
nav_order: 4
description: "Privacy-Preserving Measurement"
permalink: /dev
---
<script src="/assets/js/jquery-2.1.4.min.js"></script>

# Developer Access Information

## Design Documents
* Design documentation repo: [View on Gitlab](https://gitlab.com/cleaninsights/clean-insights-design)
* Client specification: [View on Gitlab](https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/docs/CleanInsights-ClientSpec-v0.0.1.md)

## Available Tools (Under Development)
* Android SDK: [View on Gitlab](https://gitlab.com/cleaninsights/clean-insights-android-sdk)
* iOS/MacOS SDK: [View on Gitlab](https://gitlab.com/cleaninsights/clean-insights-ios-sdk)
* Python SDK: [View on Gitlab](https://gitlab.com/cleaninsights/clean-insights-python-sdk)
* Javascript SDK: [View on Gitlab](https://gitlab.com/cleaninsights/clean-insights-js-sdk)
* Matomo Proxy (for added client privacy]: [View on Gitlab](https://gitlab.com/cleaninsights/clean-insights-matomo-proxy)

## Discussion
<a href="https://lists.mayfirst.org/mailman/listinfo/guardian-dev">Guardian-Dev Discussion List</a>: If you are a developer, designer, power user, hacker, MOD'r, cryptophreak or just anyone interested 
in following the development side, this is for you!

  The best place to chat with us live is on the <a href="https://matrix.org">Matrix network</a>. We are in the <a href="https://riot.im/app/#/room/#guardianproject:matrix.org">#guardianproject:matrix.org</a> room on Matrix, and also in the <a href="https://element.io/#/room/#cleaninsights-discussion:matrix.org">Clean Insights Public Discussion</a> room. You can most easily reach us there via the <a href="https://riot.im/app/">Matrix "Riot" web app</a>. There are also many wonderful <a href="https://matrix.org/docs/projects/try-mat
rix-now.html">Matrix apps for every OS</a>.

## Contact Us

You can always email us directly at support@guardianproject.info or use the contact form below.

<div id="feedback-form">form will be placed in here</div>

<script id="zammad_form_script" src="https://help.guardianproject.info/assets/form/form.js"></script>

<script>
$(function() {
  $('#feedback-form').ZammadForm({
    messageTitle: 'Feedback Form',
    messageSubmit: 'Submit',
    messageThankYou: 'Thank you for your inquiry (#%s)! We\'ll contact you as soon as possible.'
  });
});
</script>




